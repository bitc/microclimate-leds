#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "OctSpinProgram.hpp"

void OctSpinProgram::loop() {
  for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i] = CRGB(0, 0, 0);
  }

  for (int i = 0; i < MAX_COILS - 1; ++i) {
      int t = coils[i+1] - coils[i];
      int p1 = ((t * counter) / 512) + coils[i];
      int p2 = ((t * ((counter + 64) % 512)) / 512) + coils[i];
      int p3 = ((t * ((counter + 128) % 512)) / 512) + coils[i];
      int p4 = ((t * ((counter + 192) % 512)) / 512) + coils[i];
      int p5 = ((t * ((counter + 256) % 512)) / 512) + coils[i];
      int p6 = ((t * ((counter + 320) % 512)) / 512) + coils[i];
      int p7 = ((t * ((counter + 384) % 512)) / 512) + coils[i];
      int p8 = ((t * ((counter + 448) % 512)) / 512) + coils[i];
      CRGB color;
      if (colorCycle) {
        color = CHSV(hue, 255, 255);
      } else {
        color = CRGB(0, 128, 255);
      }
      if (p1 < NUM_LEDS) {
        leds[p1] = color;
      }
      if (p2 < NUM_LEDS) {
        leds[p2] = color;
      }
      if (p3 < NUM_LEDS) {
        leds[p3] = color;
      }
      if (p4 < NUM_LEDS) {
        leds[p4] = color;
      }
      if (p5 < NUM_LEDS) {
        leds[p5] = color;
      }
      if (p6 < NUM_LEDS) {
        leds[p6] = color;
      }
      if (p7 < NUM_LEDS) {
        leds[p7] = color;
      }
      if (p8 < NUM_LEDS) {
        leds[p8] = color;
      }
  }

  counter++;
  if (counter == 512) {
    counter = 0;

    hue++;
    if (hue == 256) {
      hue = 0;
    }    
  }
}

