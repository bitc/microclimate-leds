#include "Program.hpp"

#include <Arduino.h>
#include <EEPROM.h>

static void ResetCalibration() {
  uint16_t v = 0;
  EEPROM.put(0 * sizeof(v), v);
  v = 1;
  EEPROM.put(1 * sizeof(v), v);
  v = 2;
  EEPROM.put(2 * sizeof(v), v);
  v = 3;
  EEPROM.put(3 * sizeof(v), v);
  v = 4;
  EEPROM.put(4 * sizeof(v), v);
  v = 5;
  EEPROM.put(5 * sizeof(v), v);
  v = 6;
  EEPROM.put(6 * sizeof(v), v);
  v = 7;
  EEPROM.put(7 * sizeof(v), v);
  return;  
}

Program::Program() {
  // Uncomment the next line if this is a new Arduino:
  //ResetCalibration();

  for (int i = 0; i < MAX_COILS; ++i) {
    uint16_t v;
    EEPROM.get(i*sizeof(v), v);
    coils[i] = v;
    constexpr uint16_t FIRST_COIL_MAX = 300;
    if (i == 0) {
      if (coils[i] > FIRST_COIL_MAX) {
        coils[i] = FIRST_COIL_MAX;
      }
    } else {
      if (coils[i] <= coils[i - 1]) {
        coils[i] = coils[i - 1] + 1;
      }
    }
  }
}

