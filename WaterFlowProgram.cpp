#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "WaterFlowProgram.hpp"

WaterFlow::WaterFlow(int speed) {
  this->speed = speed;
}

void WaterFlow::loop() {
  constexpr int SPACING = 18;

  for (int i = 0; i < NUM_LEDS; ++i) {
    int phase = cycle / speed;
    if (i % SPACING == phase % SPACING) {
      leds[i].r = 0;
      leds[i].g = 0;
      leds[i].b = 255;
    } else if (i % SPACING + 1 == phase % SPACING) {
      leds[i].r = 0;
      leds[i].g = 0;
      leds[i].b = 48;
    } else if (i % SPACING + 2 == phase % SPACING) {
      leds[i].r = 8;
      leds[i].g = 0;
      leds[i].b = 16;
    } else {
      leds[i].r = 0;
      leds[i].g = 0;
      leds[i].b = 0;      
    }
  }

  cycle++;
//  if (cycle == MAX_CYCLE) {
//    cycle = 0;
//  }
}

