#include <Arduino.h>

#include "Pins.hpp"

void keypadSetup() {
  pinMode(KEYPAD_ROW_PIN_1, OUTPUT);
  pinMode(KEYPAD_ROW_PIN_2, OUTPUT);
  pinMode(KEYPAD_ROW_PIN_3, OUTPUT);
  pinMode(KEYPAD_ROW_PIN_4, OUTPUT);

  pinMode(KEYPAD_COL_PIN_1, INPUT_PULLUP);
  pinMode(KEYPAD_COL_PIN_2, INPUT_PULLUP);
  pinMode(KEYPAD_COL_PIN_3, INPUT_PULLUP);
  pinMode(KEYPAD_COL_PIN_4, INPUT_PULLUP);
}

bool no_press_flag = 0;

inline static void digitalWriteKeypadRows(uint8_t value) {
  digitalWrite(KEYPAD_ROW_PIN_1, value);
  digitalWrite(KEYPAD_ROW_PIN_2, value);
  digitalWrite(KEYPAD_ROW_PIN_3, value);
  digitalWrite(KEYPAD_ROW_PIN_4, value);
}

inline static int8_t checkButton(byte rowPin, byte colPin, int8_t returnVal) {
  digitalWrite(rowPin, HIGH);
  if(digitalRead(colPin) == HIGH) {
      no_press_flag = 0;
      digitalWriteKeypadRows(LOW);
      return returnVal;
  }
  return -1;
}

int8_t keypad() // function used to detect which button is used 
{
  if (digitalRead(KEYPAD_COL_PIN_1) == HIGH &&
    digitalRead(KEYPAD_COL_PIN_2) == HIGH &&
    digitalRead(KEYPAD_COL_PIN_3) == HIGH &&
    digitalRead(KEYPAD_COL_PIN_4) == HIGH) {
            no_press_flag=1;
    }

    if(no_press_flag==1) {
        digitalWriteKeypadRows(LOW);

        int8_t r;

        if(digitalRead(KEYPAD_COL_PIN_1) != HIGH) {
          r = checkButton(KEYPAD_ROW_PIN_1, KEYPAD_COL_PIN_1, 0 * 4 + 0);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_2, KEYPAD_COL_PIN_1, 1 * 4 + 0);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_3, KEYPAD_COL_PIN_1, 2 * 4 + 0);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_4, KEYPAD_COL_PIN_1, 3 * 4 + 0);
          if (r >= 0) return r;
        }

        if(digitalRead(KEYPAD_COL_PIN_2) != HIGH) {
          r = checkButton(KEYPAD_ROW_PIN_1, KEYPAD_COL_PIN_2, 0 * 4 + 1);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_2, KEYPAD_COL_PIN_2, 1 * 4 + 1);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_3, KEYPAD_COL_PIN_2, 2 * 4 + 1);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_4, KEYPAD_COL_PIN_2, 3 * 4 + 1);
          if (r >= 0) return r;
        }

        if(digitalRead(KEYPAD_COL_PIN_3) != HIGH) {
          r = checkButton(KEYPAD_ROW_PIN_1, KEYPAD_COL_PIN_3, 0 * 4 + 2);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_2, KEYPAD_COL_PIN_3, 1 * 4 + 2);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_3, KEYPAD_COL_PIN_3, 2 * 4 + 2);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_4, KEYPAD_COL_PIN_3, 3 * 4 + 2);
          if (r >= 0) return r;
        }

        if(digitalRead(KEYPAD_COL_PIN_4) != HIGH) {
          r = checkButton(KEYPAD_ROW_PIN_1, KEYPAD_COL_PIN_4, 0 * 4 + 3);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_2, KEYPAD_COL_PIN_4, 1 * 4 + 3);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_3, KEYPAD_COL_PIN_4, 2 * 4 + 3);
          if (r >= 0) return r;
          r = checkButton(KEYPAD_ROW_PIN_4, KEYPAD_COL_PIN_4, 3 * 4 + 3);
          if (r >= 0) return r;
        }
    }

    return -1;
}

unsigned long kdelay = 0;

int8_t keypadGetKey() {
  constexpr unsigned long period = 50;

  // used to make non-blocking delay
  if(millis() - kdelay > period) {
      kdelay = millis();
      return keypad();
  } else {
    return -1;
  }
}

