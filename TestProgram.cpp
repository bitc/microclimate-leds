#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "TestProgram.hpp"

void TestProgram::loop() {
  constexpr int SPACING = 24;

  if (mode == MODE_GRAY) {
      for (int i = 0; i < NUM_LEDS; ++i) {
        leds[i] = CRGB(6, 6, 6);
      }
  } else {
      for (int i = 0; i < NUM_LEDS; ++i) {
        if (i % SPACING == 0) {
          switch (mode) {
            case MODE_RED:
              leds[i] = CRGB(200, 0, 0);
              break;
            case MODE_GREEN:
              leds[i] = CRGB(0, 200, 0);
              break;
            case MODE_BLUE:
              leds[i] = CRGB(0, 0, 200);
              break;
          }
        } else {
          leds[i] = CRGB(0, 0, 0);
        }
      }
  }

  cycle++;
  if (cycle == MAX_CYCLE) {
    cycle = 0;
    mode++;
    if (mode == NUM_MODES) {
      mode = 0;
    }
  }
}

