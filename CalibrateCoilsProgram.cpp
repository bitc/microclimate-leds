#include <Arduino.h>
#include <FastLED.h>
#include <EEPROM.h>

#include "Pins.hpp"

#include "CalibrateCoilsProgram.hpp"

void CalibrateCoilsProgram::loop() {
  for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i] = CRGB(0, 0, 0);
  }

  constexpr int COUNTER_MAX = 24;

  for (int i = 0; i < MAX_COILS; ++i) {
    if (i == selectedCoil) {
      int v = 64 + (((256 - 64) * counter) / COUNTER_MAX);
      leds[coils[i]] = CRGB(v, v, v);
    } else {
      if (i == 0) {
        leds[coils[i]] = CRGB(255, 64, 0);
      } else {
        leds[coils[i]] = CRGB(255, 0, 0);        
      }
    }
  }

  counter++;
  if (counter == COUNTER_MAX) {
    counter = 0;
  }
}

void CalibrateCoilsProgram::buttonSelectNext() {
  selectedCoil++;
  if (selectedCoil == MAX_COILS) {
    selectedCoil = 0;
  }
}

void CalibrateCoilsProgram::buttonSelectPrev() {
  selectedCoil--;
  if (selectedCoil < 0) {
    selectedCoil = MAX_COILS - 1;
  }
}

static constexpr uint16_t MAX_COIL_VAL = NUM_LEDS + 10;

void CalibrateCoilsProgram::adjustCoils() {
  for (int i = 1; i < MAX_COILS; ++i) {
    if (coils[i] <= coils[i - 1]) {
      coils[i] = coils[i - 1] + 1;
    }
  }  
}

void CalibrateCoilsProgram::saveCoilsVals() {
  for (int i = 0; i < MAX_COILS; ++i) {
    uint16_t v = coils[i];
    EEPROM.update(i * sizeof(v), v);
  }
}

void CalibrateCoilsProgram::buttonForward() {
  if (coils[selectedCoil] < MAX_COIL_VAL) {
    coils[selectedCoil]++;
    adjustCoils();
    saveCoilsVals();
  }
}

void CalibrateCoilsProgram::buttonBackward() {
  if (coils[selectedCoil] > 0) {
    coils[selectedCoil]--;
    adjustCoils();
    saveCoilsVals();
  }
}

