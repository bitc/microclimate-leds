#pragma once

#include "Program.hpp"

class WaterFlow : public Program {
   public:
    WaterFlow(int speed);
    void loop() override;

  private:
    static constexpr int MAX_CYCLE = 10000;

    int cycle = 0;
    int speed;
};

