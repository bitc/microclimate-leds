#include <FastLED.h>

// ----------------------------------------------------
// IO Devices Connected to the Arduino
// ----------------------------------------------------

// LED strip data output pin:
constexpr int LEDS_PIN = 12;

// Sound sensor module:
constexpr int SOUND_ANALOG_PIN = A0;
constexpr int SOUND_DIGITAL_PIN = A1;

// Vibration sensor module:
// Note: Vibration sensor module emits a very short pulse of a LOW value when it senses a vibration
constexpr int VIBRATION_PIN = 11;

// Distance sensor module #1:
constexpr int DISTANCE1_TRIGGER_PIN = A2;
constexpr int DISTANCE1_ECHO_PIN = A3;

// Distance sensor module #2:
constexpr int DISTANCE2_TRIGGER_PIN = A4;
constexpr int DISTANCE2_ECHO_PIN = A5;

// 4x4 Keypad input module:
constexpr byte KEYPAD_ROW_PIN_1 = 5;
constexpr byte KEYPAD_ROW_PIN_2 = 4;
constexpr byte KEYPAD_ROW_PIN_3 = 3;
constexpr byte KEYPAD_ROW_PIN_4 = 2;

constexpr byte KEYPAD_COL_PIN_1 = 6;
constexpr byte KEYPAD_COL_PIN_2 = 7;
constexpr byte KEYPAD_COL_PIN_3 = 8;
constexpr byte KEYPAD_COL_PIN_4 = 9;

constexpr int NUM_LEDS = 486;
extern CRGB leds[NUM_LEDS];

