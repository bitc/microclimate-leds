#pragma once

#include "Program.hpp"

class BlinkProgram : public Program {
    void loop() override;
  private:
    static constexpr int COUNTER_MAX = 20;
    bool light = false;
    int counter = COUNTER_MAX;
};
