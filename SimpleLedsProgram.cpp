#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "SimpleLedsProgram.hpp"

void SimpleLedsProgram::loop() {
  leds[49].r = 0;
  leds[49].g = 128;
  leds[49].b = 0;
}
