#include <Arduino.h>

#include "Pins.hpp"
#include "BlinkProgram.hpp"

void BlinkProgram::loop() {
  counter--;
  if (counter == 0) {
    counter = COUNTER_MAX;
    light = !light;
    if (light) {
      digitalWrite(LED_BUILTIN, HIGH);
    } else {
      digitalWrite(LED_BUILTIN, LOW);      
    }
  }

  for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i].r = 0;
    leds[i].g = 0;
    leds[i].b = 0;
  }  
}
