#pragma once

#include "Program.hpp"

class OctSpinProgram : public Program {
   public:
    OctSpinProgram(bool colorCycle)
      : colorCycle(colorCycle) {
    }
    void loop() override;

  private:
    const bool colorCycle;
    int counter = 0;
    int hue = 0;
};

