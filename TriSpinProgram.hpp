#pragma once

#include "Program.hpp"

class TriSpinProgram : public Program {
   public:
    TriSpinProgram(bool colorCycle)
      : colorCycle(colorCycle) {
    }
    void loop() override;

  private:
    const bool colorCycle;
    int counter = 0;
    int hue = 0;
};

