#pragma once

#include "Program.hpp"

class RainbowProgram : public Program {
   public:
    void loop() override;

  private:
    static constexpr int MAX_CYCLE = 10000;

    int cycle = 0;
};

