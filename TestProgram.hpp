#pragma once

#include "Program.hpp"

class TestProgram : public Program {
   public:
    void loop() override;

  private:
    static constexpr int MAX_CYCLE = 256;

    int cycle = 0;

    static constexpr uint8_t MODE_GRAY = 0;
    static constexpr uint8_t MODE_RED = 1;
    static constexpr uint8_t MODE_GREEN = 2;
    static constexpr uint8_t MODE_BLUE = 3;
    static constexpr uint8_t NUM_MODES = 4;

    uint8_t mode = 0;
};

