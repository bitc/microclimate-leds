#pragma once

// Must be called from the Arduino "setup" function
void keypadSetup();

// Returns the key that was just pressed [0..15], or -1 if no key was just pressed
int8_t keypadGetKey();
