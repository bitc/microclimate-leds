#pragma once

#include "Program.hpp"

class CalibrateCoilsProgram : public Program {
   public:
    void loop() override;

    void buttonSelectPrev();
    void buttonSelectNext();
    void buttonForward();
    void buttonBackward();

  private:
    int8_t selectedCoil = 0;

    void adjustCoils();
    void saveCoilsVals();

    int counter = 0;
};

