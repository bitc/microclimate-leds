#pragma once

#include <Arduino.h>

class Program {
  public:
    Program();
    virtual ~Program() {};
    virtual void loop() = 0;

    static constexpr int MAX_COILS = 8;
    uint16_t coils[MAX_COILS];
};

