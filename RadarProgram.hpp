#pragma once

#include "Program.hpp"

class RadarProgram : public Program {
   public:
    void loop() override;

  private:
    int counter = 0;
};

