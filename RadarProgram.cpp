#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "RadarProgram.hpp"

void RadarProgram::loop() {
  for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i] = CRGB(0, 0, 0);
  }

  int ci = 0;
  for (int i = coils[0]; i <= coils[MAX_COILS - 1]; ++i) {
      if (i == coils[ci + 1]) {
          ci++;
      }
      int end = coils[ci + 1];
      int start = coils[ci];
      int t = end - start;
      int alpha = ((i - start) * 256) / t;
      int d = (counter + 256 - alpha) % 256;
      if (d <= 48) {
          leds[i] = CHSV(13, 255, (48 - d) * 5);
      }
  }

  counter++;
  if (counter == 256) {
    counter = 0;
  }
}

