#include <FastLED.h>

#include "Pins.hpp"
#include "Keypad4x4.hpp"

#include "TestProgram.hpp"
#include "BlinkProgram.hpp"
#include "SimpleLedsProgram.hpp"
#include "WaterFlowProgram.hpp"
#include "RainbowProgram.hpp"
#include "CalibrateCoilsProgram.hpp"
#include "TriSpinProgram.hpp"
#include "OctSpinProgram.hpp"
#include "RadarProgram.hpp"

// declaration of placement new:
inline void *operator new(size_t, void *buf) { return buf; }

// ----------------------------------------------------

CRGB leds[NUM_LEDS];

// ----------------------------------------------------

class ProgramManager {
  public:
    ProgramManager();

    void keypadPress(int index);

    void loop() {
      getProgramStorage()->loop();

      for (int i = 0; i < NUM_LEDS; ++i) {
        leds[i] %= brightness;
      }
      
      FastLED.show();
      delay(1);
    }

    void changeBrightness(int delta) {
      if ((int)brightness + delta > 255) {
        brightness = 255;
      } else if ((int)brightness + delta < 0) {
        brightness = 0;
      } else {
        brightness += delta;
      }
      if (brightness < 1) {
        brightness = 1;
      }
    }

  public:
    bool calibrating = false;

  private:
    Program* getProgramStorage() { return reinterpret_cast<Program*>(&programStorage[0]); }

    long long programStorage[3]; // long long to ensure a valid alignment

    uint8_t brightness = 255;

    static_assert(sizeof(TestProgram) <= sizeof(programStorage), "TestProgram sizeof");
    static_assert(sizeof(BlinkProgram) <= sizeof(programStorage), "BlinkProgram sizeof");
    static_assert(sizeof(WaterFlow) <= sizeof(programStorage), "WaterFlow sizeof");
    static_assert(sizeof(SimpleLedsProgram) <= sizeof(programStorage), "SimpleLedsProgram sizeof");
    static_assert(sizeof(RainbowProgram) <= sizeof(programStorage), "RainbowProgram sizeof");
    static_assert(sizeof(CalibrateCoilsProgram) <= sizeof(programStorage), "CalibrateCoilsProgram sizeof");
    static_assert(sizeof(TriSpinProgram) <= sizeof(programStorage), "TriSpinProgram sizeof");
    static_assert(sizeof(OctSpinProgram) <= sizeof(programStorage), "OctSpinProgram sizeof");
    static_assert(sizeof(RadarProgram) <= sizeof(programStorage), "RadarProgram sizeof");
};

ProgramManager::ProgramManager() {
  // Initial Program:
  new (getProgramStorage()) TestProgram();
//  new (getProgramStorage()) CalibrateCoilsProgram(); calibrating = true;
}

void ProgramManager::keypadPress(int index) {
  if (calibrating) {
    switch (index) {
    case 12:
      static_cast<CalibrateCoilsProgram*>(getProgramStorage())->buttonSelectPrev();
      return;
    case 13:
      static_cast<CalibrateCoilsProgram*>(getProgramStorage())->buttonSelectNext();
      return;
    case 14:
      static_cast<CalibrateCoilsProgram*>(getProgramStorage())->buttonBackward();
      return;
    case 15:
      static_cast<CalibrateCoilsProgram*>(getProgramStorage())->buttonForward();
      return;      
    }
  }

  calibrating = false;
  static_cast<Program*>(getProgramStorage())->~Program();
  switch (index) {
    case 0:
      new (getProgramStorage()) TestProgram();
      break;
    case 1:
      new (getProgramStorage()) BlinkProgram();
      break;
    case 2:
      new (getProgramStorage()) WaterFlow(3);
      break;
    case 3:
      new (getProgramStorage()) WaterFlow(2);
      break;
    case 4:
      new (getProgramStorage()) WaterFlow(1);
      break;
    case 5:
      new (getProgramStorage()) SimpleLedsProgram();
      break;
    case 6:
      new (getProgramStorage()) RainbowProgram();
      break;
    case 7:
      new (getProgramStorage()) TriSpinProgram(false);
      break;
    case 8:
      new (getProgramStorage()) TriSpinProgram(true);
      break;
    case 9:
      new (getProgramStorage()) RadarProgram();
      break;
    case 10:
      new (getProgramStorage()) OctSpinProgram(false);
      break;
    case 11:
      new (getProgramStorage()) OctSpinProgram(true);
      break;

    case 12:
      new (getProgramStorage()) CalibrateCoilsProgram();
      calibrating = true;
      break;
    case 13:
      new (getProgramStorage()) CalibrateCoilsProgram();
      calibrating = true;
      break;

    case 14:
      changeBrightness(-8);
      break;
    case 15:
      changeBrightness(8);
      break;
    default:
      new (getProgramStorage()) BlinkProgram();
      break;
  }
}

// ----------------------------------------------------

ProgramManager globalProgramManager;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  
  pinMode(LEDS_PIN, OUTPUT);
  pinMode(SOUND_ANALOG_PIN, INPUT);
  pinMode(SOUND_DIGITAL_PIN, INPUT);
  pinMode(VIBRATION_PIN, INPUT);
  pinMode(DISTANCE1_TRIGGER_PIN, OUTPUT);
  pinMode(DISTANCE1_ECHO_PIN, INPUT);
  pinMode(DISTANCE2_TRIGGER_PIN, OUTPUT);
  pinMode(DISTANCE2_ECHO_PIN, INPUT);

  keypadSetup();
  FastLED.addLeds<NEOPIXEL, LEDS_PIN>(leds, NUM_LEDS);
}

long counter = 100;

void loop() {
  int8_t key = keypadGetKey();
  if (key >= 0) {
    globalProgramManager.keypadPress(key);
  }

  globalProgramManager.loop();
/*
int val = digitalRead(VIBRATION_PIN);
if (val == LOW) {
  counter = 2000;
}

if (counter > 0) {
 digitalWrite(LED_BUILTIN, HIGH);
 counter--;
} else {
  digitalWrite(LED_BUILTIN, LOW);
}

//Serial.println(val);
//Serial.print(digitalRead(SOUND_DIGITAL_PIN));
//Serial.print(' ');
//Serial.println(analogRead(SOUND_ANALOG_PIN));
*/
}
