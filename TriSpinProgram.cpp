#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "TriSpinProgram.hpp"

void TriSpinProgram::loop() {
  for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i] = CRGB(0, 0, 0);
  }

  for (int i = 0; i < MAX_COILS - 1; ++i) {
      int t = coils[i+1] - coils[i];
      int p1 = ((t * counter) / 256) + coils[i];
      int p2 = ((t * ((counter + 85) % 256)) / 256) + coils[i];
      int p3 = ((t * ((counter + 171) % 256)) / 256) + coils[i];
      CRGB color;
      if (colorCycle) {
        color = CHSV(hue, 255, 255);
      } else {
        color = CRGB(0, 128, 255);
      }
      if (p1 < NUM_LEDS) {
        leds[p1] = color;
      }
      if (p2 < NUM_LEDS) {
        leds[p2] = color;
      }
      if (p3 < NUM_LEDS) {
        leds[p3] = color;
      }
  }

  counter++;
  if (counter == 256) {
    counter = 0;

    hue++;
    if (hue == 256) {
      hue = 0;
    }    
  }
}

