#include <Arduino.h>
#include <FastLED.h>

#include "Pins.hpp"

#include "RainbowProgram.hpp"

void RainbowProgram::loop() {
  constexpr int SPACING = 18;
  int phase = cycle / 2;
  int pulsePhase = cycle / 4;

  for (int i = 0; i < NUM_LEDS; ++i) {
    int val = quadwave8((i*16) % 256);
    leds[i] = CHSV(((i + phase)*16) % 256, 255, val);
//    leds[i] %= cubicwave8(pulsePhase % 256);
  }

  cycle++;
  if (cycle == MAX_CYCLE) {
    cycle = 0;
  }
}

